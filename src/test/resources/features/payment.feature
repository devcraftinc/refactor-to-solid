Feature: Payment
  I want to pay so I can go home

  Scenario Outline: Per hour payment
    Given I entered the parking at <entry_time>
    When I pay at <payment_time>
    Then I should pay <amount_to_pay>
    Examples:
      | day_of_week | entry_time          | payment_time        | amount_to_pay | comment                            |
      | Sunday      | 0001-01-01 10:00:00 | 0001-01-01 10:00:01 | 12            | every part of first hour is 1 hour |
      | Sunday      | 0001-01-01 10:00:00 | 0001-01-01 10:59:59 | 12            | every part of first hour is 1 hour |
      | Sunday      | 0001-01-01 10:00:00 | 0001-01-01 11:00:00 | 15            | every next 15 min is 3             |
      | Sunday      | 0001-01-01 10:00:00 | 0001-01-01 11:15:00 | 18            | every next 15 min is 3             |

package devcraft.parking;


import devcraft.parking.jpa.ParkingEntryRepository;

import java.time.Duration;
import java.time.Instant;

import static java.time.Duration.between;

public class PaymentService {

    private ParkingEntryRepository repository;
    private Clock clock;

    public interface Clock {
        long now();
    }

    public PaymentService(Clock clock, ParkingEntryRepository repository) {
        this.clock = clock;
        this.repository = repository;
    }

    public long enterParking() {
        ParkingEntryRepository.ParkingEntry entry = new ParkingEntryRepository.ParkingEntry();
        long entryTime = clock.now();
		entry.setTime(entryTime);
        repository.save(entry);
        return entry.getCode();
    }

    public int calcPayment(long code) {
    	long paymentTime = clock.now();
        ParkingEntryRepository.ParkingEntry entry = repository.findOne(code);
        long entryTime = entry.getTime();
		return calcPayment(Instant.ofEpochMilli( entryTime), Instant.ofEpochMilli(paymentTime));
    }

    private int calcPayment(Instant entryTime, Instant paymentTime) {
        Duration timeInParking = between(entryTime, paymentTime);
        int amountToPay = 12;
        if (!isLessThanAnHour(timeInParking)) {
            long intervalsToPay = calcIntervalsToPay(timeInParking.toMillis());
            amountToPay += (intervalsToPay * 3);
        }
        return amountToPay;
    }

    private boolean isLessThanAnHour(Duration timeInParking) {
        return timeInParking.toMillis() < Duration.ofMinutes(60).toMillis();
    }

    private long calcIntervalsToPay(long timeInParking) {
        timeInParking -= minAsMillis(60);
        timeInParking += minAsMillis(15);
        return timeInParking / minAsMillis(15);
    }

    private long minAsMillis(int min) {
        return min * 60 * 1000L;
    }

}